# nxostoolkit

## Requirements

* requests

## Usage

This module contains a class named __Nexus__ (modified from the original assignement to adhere to PEP8 standards).

The following methods are publicly available:

* __constructor__ : set the IP address / URL and port of the Nexus switch (optionally specify if HTTPS should be used)
* __authenticate__ : authenticate with the switch with a username and password
* __get_interface_status__ : given an interface name, returns the administrative status of the interface
* __configure_interface_desc__ : given an interface name and description, updates the interface description on the remote switch
* __get_interface_desc__ : given an interface name, return its description

