import requests
import json
import re

"""This module provides a basic interaction with a Nexus switch API."""


class Nexus:
    __headers = {"Content-Type": "application/json", "Cache-Control": "no-cache"}
    __headers_jsonrpc = {"Content-Type": "application/json-rpc"}

    def __init__(self, ip_address: str, port: str, https: bool = False):
        """Default constructor. Creates a Nexus object with the empty version and platform information.
        Version and platform information are automatically populated upon first authentication
        """
        self.__auth = None
        self.__cookie = None

        self.__ip_address = ip_address
        self.__port = port
        if https:
            self.__uri = 'https://{}:{}'.format(ip_address, port)
        else:
            self.__uri = 'http://{}:{}'.format(ip_address, port)

        self.__version = ""
        self.__platform = ""

    # TODO manage cookie expiration
    def authenticate(self, username: str, password: str) -> None:
        """Authenticates the user to the Nexus switch using the provided credentials."""
        self.__auth = (username, password)

        payload = {
            "aaaUser": {
                "attributes": {
                    "name": username,
                    "pwd": password
                }
            }
        }

        response = requests.post(self.__uri + "/api/aaaLogin.json",
                                 data=json.dumps(payload),
                                 headers=Nexus.__headers)

        if response.status_code == 200:
            try:
                # store the cookie for future operations
                self.__cookie = {"APIC-cookie": response.cookies["APIC-cookie"]}
            except KeyError:
                # authentication was not successful
                self.__cookie = None

            # Execute second request to get version and platform
            # response = self.__nxapi_get("/api/mo/sys/fwstatuscont/running.json")  # y u no work?!
            response = self.__jsonrpc("show version")
            if response.status_code == 200:
                response_data = json.loads(response.text)
                self.__version = response_data['result']['body']['kickstart_ver_str']
                self.__platform = response_data['result']['body']['chassis_id']

    def get_interface_status(self, interface_name: str) -> str:
        """Returns the administrative status of the given interface name."""

        # Extract interface number
        try:
            found = re.search("[0-9]*/[0-9]*", interface_name).group(0)
        except AttributeError:
            # No expected pattern found
            raise Exception("Invalid interface name given.")

        interface = "eth" + found
        response = self.__nxapi_get("/api/mo/sys/intf/phys-[{}].json".format(interface))

        if response.status_code == 200:
            response_data = json.loads(response.text)
            return response_data["imdata"][0]["l1PhysIf"]["attributes"]["adminSt"]
        else:
            return "unknown"

    def configure_interface_desc(self, interface_name: str, description: str) -> None:
        """Configures the description of the given interface."""

        # Extract interface number
        try:
            found = re.search("[0-9]*/[0-9]*", interface_name).group(0)

            interface = "eth" + found

            payload = {
                "l1PhysIf": {
                    "attributes": {
                        "descr": description,
                    }
                }
            }
            self.__nxapi_post("/api/mo/sys/intf/phys-[{}].json".format(interface), payload)

        except AttributeError:
            # No expected pattern found
            raise Exception("Invalid interface name given.")

    def get_interface_desc(self, interface_name: str) -> str:
        """Returns the description of the given interface name."""

        # Extract interface number
        try:
            found = re.search("[0-9]*/[0-9]*", interface_name).group(0)
        except AttributeError:
            # No expected pattern found
            raise Exception("Invalid interface name given.")

        interface = "eth" + found
        response = self.__nxapi_get("/api/mo/sys/intf/phys-[{}].json".format(interface))

        if response.status_code == 200:
            response_data = json.loads(response.text)
            return response_data["imdata"][0]["l1PhysIf"]["attributes"]["descr"]

    def __nxapi_get(self, endpoint: str) -> requests.models.Response:
        """Execute a GET operation based on the NX-API REST protocol."""
        if self.__cookie is not None:
            uri = self.__uri + endpoint
            return requests.get(uri,
                                headers=Nexus.__headers,
                                cookies=self.__cookie)
        else:
            raise Exception("Authenticate required before executing any requests.")

    def __nxapi_post(self, endpoint: str, payload: dict) -> requests.models.Response:
        """Execute a POST operation based on the NX-API REST protocol."""
        if self.__cookie is not None:
            uri = self.__uri + endpoint
            return requests.post(uri,
                                 data=json.dumps(payload),
                                 headers=Nexus.__headers,
                                 cookies=self.__cookie)
        else:
            raise Exception("Authenticate required before executing any requests.")

    def __jsonrpc(self, command: str) -> requests.models.Response:
        payload = [{
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
                "cmd": command,
                "version": 1
            },
            "id": 1
        }]

        return requests.post(self.__uri + "/ins",
                             data=json.dumps(payload),
                             headers=Nexus.__headers_jsonrpc,
                             auth=self.__auth)


if __name__ == '__main__':
    nexus = Nexus("192.168.1.240", "80")
    nexus.authenticate("admin", "cisco1234!")
    print(nexus._Nexus__version)
    print(nexus._Nexus__platform)
    print(nexus.get_interface_status("Eth1/1"))
    nexus.configure_interface_desc("Ethernet 1/1", "this is a description update number 4")
    print(nexus.get_interface_desc("Ethernet 1/1"))
