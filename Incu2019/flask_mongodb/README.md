# Flask Homework

The homework file is name `flask_hw.py` and uses a MongoDB that must be configured using the `flask_config.ini` file as indicated in the [MongoDB Configuration](#mongodb-configuration).

## Requirements

* flaks_mongo.py
* flask
* pymongo
* bson

## MongoDB Configuration

In order to connect to the database, you must edit the `flask_config.ini` file with the following fields:

* `ServerIp` : IP address or hostname of the MongoDB server
* `ServerPort ` : port of the MongoDB server
* `Database ` : name of the database to use on the MongoDB server
* `Collection ` : name of the collection ot use on the MongoDB server

## Endpoints

**Base URL :** http://127.0.0.1:7676/

* **GET /** - returns all elements
* **GET /<user_id>** - returns a single element
* **PUT /<user_id>** - update an existing element
* **POST /** - insert a new element, the given dictionnary must contain the fields named "name", "phone_number", "email"
* **DELETE /<user_id>** - delete an element by ID

## Web Interface

* Create a new element: http://127.0.0.1:7676/form
* View all elements: http://127.0.0.1:7676/view




# MongoDB Homework

## Requirements

* pymongo
* bson

## Usage

### Configuration

In order to connect to the database, you must edit the `config.ini` file with the following fields:

* `ServerIp` : IP address or hostname of the MongoDB server
* `ServerPort ` : port of the MongoDB server
* `Database ` : name of the database to use on the MongoDB server
* `Collection ` : name of the collection ot use on the MongoDB server

### Methods

The following methods have been implemented as requested:

* `insert_new_entry(new_entry: dict) -> None`
* `update_entry(firstname: str, lastname: str, new_values: dict) -> None`
* `display_entries(firstname: str = None, lastname: str = None) -> None`
* `delete_entries(firstname: str = None, lastname: str = None) -> None`

**NOTE:** safeguards have been built-in to avoid duplicate entries from being inserted. ValueError are raised with explicit messages.