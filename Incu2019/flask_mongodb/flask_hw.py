import bson

from flask import Flask, render_template, request, jsonify
import flask_mongo
from bson.json_util import dumps

app = Flask(__name__)


@app.route("/", methods=["GET"])
def get_all_elements():
    result = flask_mongo.get_entries()

    result_list = list()
    for i in result:
        result_list.append({
            "userid": str(i["_id"]),
            "name": i["name"],
            "email": (i["email"] if "email" in i else None),
            "phone_number": (i["phone_number"] if "phone_number" in i else None)
        })

    return jsonify(result=result_list), 200


@app.route("/<user_id>", methods=["GET"])
def get_single_element(user_id: str = None):
    print(user_id)
    try:
        result = flask_mongo.get_entries(user_id)
        result_list = list()
        for i in result:
            result_list.append({
                "userid": str(i["_id"]),
                "name": i["name"],
                "email": (i["email"] if "email" in i else None),
                "phone_number": (i["phone_number"] if "phone_number" in i else None)
            })

        return jsonify(result=result_list), 200

    except bson.errors.InvalidId:
        return jsonify(error="No entry found with user_id {}".format(user_id)), 404
    except:
        return jsonify(error="Could not process request"), 500


@app.route("/<user_id>", methods=["PUT"])
def update_element(user_id: str = None):
    if request.headers['Content-Type'] == 'application/json':
        try:
            new_entry = dict()
            if "name" in request.get_json():
                new_entry["name"] = request.get_json()["name"]
            if "phone_number" in request.get_json():
                new_entry["phone_number"] = request.get_json()["phone_number"]
            if "email" in request.get_json():
                new_entry["email"] = request.get_json()["email"]

            result = flask_mongo.update_entry(user_id, new_entry)

            if result == 1:
                return jsonify(success=True), 200
            else:
                return request.get_json(), 500
        except bson.errors.InvalidId:
            return jsonify(error="No entry found with user_id {}".format(user_id)), 404
        except ValueError as e:
            return jsonify(error=e.args), 500
    else:
        return jsonify(error="Could not process request"), 500


@app.route("/", methods=["POST"])
def insert_element():
    if request.headers['Content-Type'] == 'application/json':
        try:
            new_entry = dict()
            if "name" in request.get_json():
                new_entry["name"] = request.get_json()["name"]
            if "phone_number" in request.get_json():
                new_entry["phone_number"] = request.get_json()["phone_number"]
            if "email" in request.get_json():
                new_entry["email"] = request.get_json()["email"]

            result = flask_mongo.insert_new_entry(new_entry)

            if result is not "":
                return jsonify(inserted_id=result), 200
            else:
                return request.get_json(), 500
        except ValueError as e:
            return jsonify(error=e.args), 500

    elif request.headers['Content-Type'] == 'application/x-www-form-urlencoded':
        try:
            new_entry = dict()
            if "name" in request.form:
                new_entry["name"] = request.form["name"]
            if "phone_number" in request.form:
                new_entry["phone_number"] = request.form["phone_number"]
            if "email" in request.form:
                new_entry["email"] = request.form["email"]

            result = flask_mongo.insert_new_entry(new_entry)

            if result is not "":
                return jsonify(inserted_id=result), 200
            else:
                return request.get_json(), 500
        except ValueError as e:
            return jsonify(error=e.args), 500
    else:
        return jsonify(error="Could not process request"), 500


@app.route("/<user_id>", methods=["DELETE"])
def delete_element(user_id: str = None):
    try:
        result = flask_mongo.delete_entries(user_id)
        if result == 1:
            return jsonify(deleted_count=result), 200
        elif result == 0:
            return jsonify(error="No entry found with user_id {}".format(user_id)), 404
    except bson.errors.InvalidId:
        return jsonify(error="No entry found with user_id {}".format(user_id)), 404
    except ValueError as e:
        return jsonify(error=e.args), 500


@app.route("/form", methods=["GET"])
def form():
    return render_template("form.html")


@app.route("/view", methods=["GET"])
def view():
    result = flask_mongo.get_entries()

    result_list = list()
    for i in result:
        result_list.append({
            "userid": str(i["_id"]),
            "name": i["name"],
            "email": (i["email"] if "email" in i else None),
            "phone_number": (i["phone_number"] if "phone_number" in i else None)
        })

    return render_template("view.html", result=result_list)


app.run(host="127.0.0.1", port=7676)
