import bson
import configparser
from typing import Any

import pymongo
from bson import ObjectId
from pymongo.results import UpdateResult


def get_mongo():
    config = configparser.ConfigParser()
    config.read("flask_config.ini")

    client = pymongo.MongoClient(
        config["mongodb"].get("ServerIp"),
        int(config["mongodb"].get("ServerPort")))

    db = client[config["mongodb"].get("Database")]
    return db[config["mongodb"].get("Collection")]


def get_entries(user_id: str = None) -> list:
    table = get_mongo()
    if user_id is None:
        result = table.find()
    else:
        result = table.find({"_id": ObjectId(user_id)})

    result_list = list()

    for i in result:
        result_list.append(i)

    return result_list


def update_entry(user_id: str, new_values: dict) -> int:
    table = get_mongo()

    # Check if the id exists
    result = table.find({"_id": ObjectId(user_id)})
    if result.count() < 1:
        raise bson.errors.InvalidId("No entry found with user_id {}".format(user_id))

    update = dict()

    if "name" in new_values:
        # Check if name is already present
        result = table.find({"name": new_values["name"]})
        if result.count() >= 1 and result[0]["_id"] != ObjectId(user_id):
            raise ValueError("An entry with value 'name: {}' is already present. Duplicate "
                             "entries are not allowed".format(new_values["name"]))
        else:
            update["name"] = new_values["name"]
    if "phone_number" in new_values:
        update["phone_number"] = new_values["phone_number"]
    if "email" in new_values:
        update["email"] = new_values["email"]

    if len(update) > 0:
        result = table.update_one({"_id": ObjectId(user_id)}, {"$set": update})
        return result.matched_count
    else:
        raise ValueError("No valid entries found in new_values. Valid entries include: 'name', "
                         "'phone_number', 'email'.")


def insert_new_entry(new_entry: dict) -> str:
    table = get_mongo()

    if "name" not in new_entry or new_entry["name"] is None:
        raise ValueError("'name' is mandatory field in the new entry.")
    else:
        result = table.find({"name": new_entry["name"]})
        if result.count() >= 1:
            raise ValueError("An entry with value 'name: {}' is already present. Duplicate "
                             "entries are not allowed".format(new_entry["name"]))
        else:
            post_id = table.insert_one({
                "name": new_entry["name"]
            })

            if "email" in new_entry:
                table.update_one({"_id": post_id.inserted_id}, {"$set": {"email": new_entry["email"]}})

            if "phone_number" in new_entry:
                table.update_one({"_id": post_id.inserted_id}, {"$set": {"phone_number": new_entry["phone_number"]}})

            if post_id is not None:
                return str(post_id.inserted_id)
            else:
                return ""


def delete_entries(user_id: int) -> int:
    table = get_mongo()
    if user_id is not None:
        result = table.delete_one({"_id": ObjectId(user_id)})
        return result.deleted_count
    else:
        raise ValueError("Now user_id provided")
