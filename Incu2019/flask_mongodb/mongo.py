import configparser
import pymongo


def get_mongo():
    config = configparser.ConfigParser()
    config.read("config.ini")

    client = pymongo.MongoClient(
        config["mongodb"].get("ServerIp"),
        int(config["mongodb"].get("ServerPort")))

    db = client[config["mongodb"].get("Database")]
    return db[config["mongodb"].get("Collection")]


def insert_new_entry(new_entry: dict) -> None:
    """
    Create a new entry in the address book database.
    The new_entry directory takes the following key values:
    - firstname (mandatory)
    - lastname (mandatory)
    - email (optional)
    - home (optional - Home phone number)
    - work (optional - Work phone number)

    :param new_entry: dictionary of the new entry to insert.
    :return: None
    :raises: ValueError if the mandatory firstname and lastname fields are not present in the new_entry parameter.
    :raises: ValueError if an entry already exists with the given firstname and lastname.
    """
    table = get_mongo()

    if "firstname" not in new_entry or "lastname" not in new_entry:
        raise ValueError("'firstname' and 'lastname' are mandatory field in the new entry.")
    else:
        result = table.find({"firstname": new_entry["firstname"], "lastname": new_entry["lastname"]})

        if result.count() >= 1:
            raise ValueError("An entry with values 'firstname: {}' and 'lastname: {}' is already present. Duplicate "
                             "entries are not allowed".format(new_entry["firstname"], new_entry["lastname"]))
        else:
            post_id = table.insert_one({
                "firstname": new_entry["firstname"],
                "lastname": new_entry["lastname"],
            })

            if "email" in new_entry:
                table.update_one({"_id": post_id.inserted_id}, {"$set": {"email": new_entry["email"]}})

            if "home" in new_entry:
                table.update_one({"_id": post_id.inserted_id}, {"$set": {"home": new_entry["home"]}})

            if "work" in new_entry:
                table.update_one({"_id": post_id.inserted_id}, {"$set": {"work": new_entry["work"]}})

            print("Entry inserted successfully")


def update_entry(firstname: str, lastname: str, new_values: dict) -> None:
    """
    Update an existing entry in the database.

    :param firstname: firstname of the entry to update.
    :param lastname: lastname of the entry to update.
    :param new_values: dictionary of the values to udpate (possible fields: firstname, lastname, email, home, work).
    :return: None
    :raises: ValueError if the new_values parameter does not contain any valid fields.
    """
    table = get_mongo()

    update = dict()

    if "firstname" in new_values:
        update["firstname"] = new_values["firstname"]
    if "lastname" in new_values:
        update["lastname"] = new_values["lastname"]
    if "email" in new_values:
        update["email"] = new_values["email"]
    if "home" in new_values:
        update["home"] = new_values["home"]
    if "work" in new_values:
        update["work"] = new_values["work"]

    if len(update) > 0:
        result = table.update_one({"firstname": firstname, "lastname": lastname}, {"$set": update})
        print("Entry modified successfully")
    else:
        raise ValueError("No valid entries found in new_values. Valid entries include: 'firstname', "
                         "'lastname', 'email', 'home', 'work'.")


def display_entries(firstname: str = None, lastname: str = None) -> None:
    """
    Displays all or specific entries in the database.

    If no parameter are given, the method displays all entries in the table.
    If one of the parameter is given, the method displays the entries matching any string given using regex pattern.

    :param firstname: string to search for in the firstnames
    :param lastname: string to search for in the lastnames
    :return: None
    """
    table = get_mongo()
    if firstname is None and lastname is None:
        result = table.find()
    elif firstname is None:
        result = table.find({"lastname": {"$regex": lastname + ".*"}})
    elif lastname is None:
        result = table.find({"firstname": {"$regex": firstname + ".*"}})
    else:
        result = table.find({"firstname": {"$regex": firstname + ".*"}, "lastname": {"$regex": lastname + ".*"}})

    for i in result.sort("lastname", pymongo.ASCENDING):
        print(i)


def delete_entries(firstname: str = None, lastname: str = None) -> None:
    """
    Delete all entries containing one or both of the given parameters.

    :param firstname:
    :param lastname:
    :return: None
    """
    table = get_mongo()
    if firstname is None:
        result = table.delete_many({"lastname": lastname})
    elif lastname is None:
        result = table.delete_many({"firstname": firstname})
    else:
        result = table.delete_many({"firstname": firstname, "lastname": lastname})

    print("Deleted: {}".format(result.deleted_count))
