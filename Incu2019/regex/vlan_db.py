###################################################
# Parse vlan status
# 
# Students will need to parse the output of a "show vlan" from a NXOS devices
# and return a dictionary with the parsed info
# 
# How the text input will look like:
# 
# 
# VLAN Name                             Status    Ports
# ---- -------------------------------- --------- -------------------------------
# 1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
#                                                 Eth1/4
# 10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
#                                                 Eth1/5, Eth1/16, Eth1/17
#                                                 Eth1/18, Eth1/49, Eth1/50
# How the output must look like:
# 
# vlan_db = {
# 	"1": {
# 		"Name": "default",
# 		"Status": "active",,
# 		"Ports": ["Po213", "Eth1/1"...]
# 	},
# 	"2":{},
# 	...
# }
#
###################################################
import re
import nexus_toolkit


def get_vlan_db(text):
    """
        Given an ASCII formated VLAN database table from the Nexus switch, returns a dictionary with the fields
        and their corresponding information
        :param text: ASCII VLAN database output from Nexus switch
        :return: dict corresponding to the VLAN database table
        """
    # Create a list of fields in the table
    fields_regex = r"^VLAN.*?$"
    matches = re.finditer(fields_regex, text, re.IGNORECASE | re.MULTILINE)
    fields = []
    for item in matches:
        temp_list = item.group().split()
        fields = fields + (temp_list[1:])

    sub_regex = r","
    substitute = ""
    result = re.sub(sub_regex, substitute, text, 0, re.MULTILINE | re.IGNORECASE)

    regex = r"^\d.*\n*?(\W*Eth\d*/\d*)*"
    matches = re.finditer(regex, result, re.IGNORECASE | re.MULTILINE)
    full_list = []
    for item in matches:
        full_list.append(item.group().split())

    if len(full_list) > 0:
        vlan_dict = dict()
        for item in full_list:
            name = item[0]
            vlan_dict[name] = dict()
            vlan_dict[name][fields[0]] = item[1]
            vlan_dict[name][fields[1]] = item[2]
            vlan_dict[name][fields[2]] = item[3:]

    return vlan_dict


def unit_test() -> dict:
    """
    Used to quickly test the get_error_counters function.
    :return: dict returned by get_error_counters
    """
    nexus = nexus_toolkit.Nexus("192.168.1.240", 80, https=False)
    nexus.authenticate("admin", "cisco1234!")
    vlans_ascii = nexus.get_vlan_database()
    vlans_dict = get_vlan_db(vlans_ascii)
    return vlans_dict


if __name__ == "__main__":
    print(unit_test())
