###################################################
# Duplicate words 
# The students will have to find the duplicate words in the given text,
# and return the string without duplicates.
#
###################################################

import re


def remove_duplicates(text: str):
    """
    Searches the input string for duplicate words (seperated by space or not) and removes unnecessary duplicates.
    :param text: input string
    :return: input text without any duplicates
    """

    regex = r"(\b\w+[\b]?)(\W*\1\b)"
    substitute = "\\1"

    result = re.sub(regex, substitute, text, 0, re.MULTILINE | re.IGNORECASE)

    if result:
        return result
    else:
        return text
