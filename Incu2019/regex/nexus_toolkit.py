import requests
import json

"""This package is an adapted version of the nxapi homework, modified to include methods to get the required output 
for the regex homework 3 and 4."""

class Nexus:
    __headers = {"Content-Type": "application/json-rpc"}

    def __init__(self, ip_address: str, port: str, https: bool = False):
        """Default constructor. Creates a Nexus object with the empty version and platform information.
        Version and platform information are automatically populated upon first authentication
        """
        self.__auth = None
        self.__ip_address = ip_address
        self.__port = port
        if https:
            self.__uri = 'https://{}:{}'.format(ip_address, port)
        else:
            self.__uri = 'http://{}:{}'.format(ip_address, port)

    def authenticate(self, username: str, password: str) -> None:
        """Authenticates the user to the Nexus switch using the provided credentials."""
        self.__auth = (username, password)

    def get_error_counters(self) -> str:
        """
        Get the CRC error counters in ASCII form from the switch.
        :return: string containing the table of CRC error counters
        """

        response = self.__jsonrpc("show interface counters errors")
        if response.status_code == 200:
            return json.loads(response.text)["result"]["msg"]
        else:
            return ""

    def get_vlan_database(self) -> str:
        """
        Get the VLAN database in ASCII form from the switch.
        :return: string containing the vlan database table
        """

        response = self.__jsonrpc("show vlan brief")
        if response.status_code == 200:
            return json.loads(response.text)["result"]["msg"]
        else:
            return ""

    def __jsonrpc(self, command: str) -> requests.models.Response:
        payload = [{
            "jsonrpc": "2.0",
            "method": "cli_ascii",
            "params": {
                "cmd": command,
                "version": 1
            },
            "id": 1
        }]

        return requests.post(self.__uri + "/ins",
                             data=json.dumps(payload),
                             headers=Nexus.__headers,
                             auth=self.__auth)


