###################################################
# Parse internal CRC counters of a Nexus
# The students will have to parse the next output, and return a dictionary,
# with the parsed info
# 
# How the text input will look like:
#
# --------------------------------------------------------------------------------
# Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# Eth1/3                0          0          0          0          0           0
# Eth1/4                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
# --------------------------------------------------------------------------------
# Eth1/1             0          --           0           0           0          0
# Eth1/2             0          --           0           0           0          0
#
# How the output must look like:
#
# int_dict = {
# 	"Eth1/1": {
# 		"Align-Err": 0,
# 		"FCS-Err": 0,
# 		...
# 	}
# }
###################################################

import nexus_toolkit
import re


def get_error_counters(text: str) -> dict:
    """
    Given an ASCII formated table of CRC counters from the Nexus switch, returns a dictionnary with the fields
    and their corresponding counters.
    :param text: ASCII CRC error counters output from Nexus switch
    :return: dict corresponding to the CRC error counters table
    """
    # Create a list of fields in the table
    regex = r"^Port.*?$"
    matches = re.finditer(regex, text, re.IGNORECASE | re.MULTILINE)
    fields = []
    for item in matches:
        temp_list = item.group().split()
        fields = fields + (temp_list[1:])

    regex = r"^Eth\d/\d.*?\d$"
    matches = re.finditer(regex, text, re.IGNORECASE | re.MULTILINE)
    full_list = []
    for item in matches:
        full_list.append(item.group().split())

    # We assume the number of interfaces is = for all 3 tables
    nbr_interfaces = len(full_list) // 3
    nbr_cols = len(fields) // 3

    errors_dict = dict()
    cnt = 0
    for item in full_list:
        fields_cnt = cnt // nbr_interfaces
        interface = item[0]
        errors = item[1:]
        index = 0
        if interface not in errors_dict:
            errors_dict[interface] = dict()

        for sub_item in errors:
            try:
                errors_dict[interface][fields[(fields_cnt * nbr_cols) + index]] = int(sub_item)
            except ValueError:
                errors_dict[interface][fields[(fields_cnt * nbr_cols) + index]] = sub_item
            index += 1
        cnt += 1

    return errors_dict


def unit_test() -> dict:
    """
    Used to quickly test the get_error_counters function.
    :return: dict returned by get_error_counters
    """
    nexus = nexus_toolkit.Nexus("192.168.1.240", 80, https=False)
    nexus.authenticate("admin", "cisco1234!")
    error_ascii = nexus.get_error_counters()
    errors_dict = get_error_counters(error_ascii)
    return errors_dict


if __name__ == "__main__":
    print(unit_test())
