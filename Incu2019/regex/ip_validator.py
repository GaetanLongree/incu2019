###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################

import re


def is_valid_ip(ip: str):
    """
    Determines whether a given IP address is valid or not
    :param ip: Must be passed as string one at a time (otherwise raises a ValueError exception)
    :return: True or False
    """

    regex = r"\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3}"
    match = re.findall(regex, ip, re.IGNORECASE | re.MULTILINE)

    if len(match) is 1:
        ip_list = match[0].split('.')
        for ip_part in ip_list:
            if int(ip_part) > 255:
                return False
        else:
            return True
    elif len(match) > 1:
        raise ValueError("IP addresses must be given one at a time")
    else:
        return False


def get_ip_class(ip: str):
    """
    Determines the IP address class of the given IP
    :param ip: Must be passed as string one at a time (otherwise raises a ValueError exception)
    :return: String
    """

    try:
        if is_valid_ip(ip):
            regex = r"^\d{1,3}"
            match = re.search(regex, ip, re.IGNORECASE | re.MULTILINE)

            if match is not None:
                found_class = ""
                ip_list = match[0].split('.')
                first_octet = int(ip_list[0])
                if 1 <= first_octet <= 127:
                    found_class = "A"
                elif 128 <= first_octet <= 191:
                    found_class = "B"
                elif 192 <= first_octet <= 223:
                    found_class = "C"
                elif 224 <= first_octet <= 239:
                    found_class = "D"
                elif 240 <= first_octet <= 255:
                    found_class = "E"
                else:
                    return ip + " is classless IP"
                return ip + " is a class " + found_class + " IP"
        else:
            return ip + " is not a valid IP address"
    except Exception:
        raise
